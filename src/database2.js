var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE soldrops (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            project text, 
            launch text, 
            amount number, 
            mint decimal,
            link1 text, 
            link2 text, 
            link3 text, 
            CONSTRAINT project_unique UNIQUE (project)
            )`,
        (err) => {
            if (err) {
                // Table already created
            }else{
                var insert = 'INSERT INTO soldrops (project, launch, amount, mint, link1, link2, link3) VALUES (?,?,?,?,?,?,?)'
                db.run(insert, ["Spirits of Solana 1","3 PM UTC",4999,0.5,"index.html","index.html","index.html"])
                db.run(insert, ["Spirits of Solana 2","3 PM UTC",4999,0.5,"index.html","index.html","index.html"])
                db.run(insert, ["Spirits of Solana 3","3 PM UTC",4999,0.5,"index.html","index.html","index.html"])
            }
        });  
    }
});


module.exports = db
