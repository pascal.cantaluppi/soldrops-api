require('dotenv').config()

var express = require("express")
var app = express()

const dropsRoute = require('./routes/drops-route')

var HTTP_PORT = process.env.PORT || 3000; 

app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});

app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

app.use('/drops', dropsRoute)

app.use(function(req, res){
    res.status(404);
});