var db = require("../database.js")

exports.drops = async (req, res) => {
    var sql = "select * from soldrops"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
};