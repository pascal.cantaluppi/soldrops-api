const express = require('express')

const dropsController = require('./../controllers/drops-controller.js')

const router = express.Router()

router.get('/', dropsController.drops)

module.exports = router